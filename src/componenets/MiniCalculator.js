import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {
  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> 0 </span>
        </div>
        <div className="operations">
          <button>加1</button>
          <button>减1</button>
          <button>乘以2</button>
          <button>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

